
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WEEK EIGHT TASK</title> 

    <style type="text/css">

        html,body{margin: 0px;}
        .content{
            background-color: #DDD;
            width: 100%;
            margin: 0px auto;
            font-family: sans-serif;
            text-indent: 12px;
        }

        .details{
            border-top: 1px solid #000;
            border-bottom: 1px solid #000;
            background-color: grey;
            margin: 0px;
        }

        .formular{
            z-index: 1;
            top: 40%;
            left: 45%;
            border: 1px solid #000;
            border-radius: 10%;
            width: 50%;
            height: auto;
            background-color: grey;
            color: #000;
            position: fixed;
            
        }
    </style>  
</head>

<body>
    <main class="content"> 
        <?php 
            echo '<h2 class="details">A SIMPLE PROGRAM SHOWING DATES OUTPUT, INDICATING A LEAP YEAR FROM 1980 THROUGH 2018</h2>';                    
        ?>
        <div class="formular">
            <p>Logic used to compute:</p>
            <ul>
            There are 3 steps to find a leap year:
            <li>  year % 4 == 0  --> Evenly divisible by 4 </li>
            <li>  AND year % 100 != 0  --> Should not be evenly divisible by 4 </li>
            <li>  OR year % 400 == 0  --> Evenly divisible by 400 </li>

            (if 1st and 2nd  steps are BOTH TRUE, OR 3rd is TRUE, THEN it's a Leap year).
            </ul>
        </div>

        <?php
            
            echo '<p>Calculating... </p>';

            //Set leap year counter
            $counter = 0;
            //Declare your start and stop year
            $startYear = '1980';
            $stopYear = '2018';


            // Run the for Loop to print the years through start and stop year
            while( $startYear <= $stopYear){

                // check conditions for a valid leap year
                if (($startYear % 4 == 0) || ($startYear % 400 == 0) && ($startYear % 100 != 0))
                {
                    echo '<h5>' . $startYear . '&nbsp; is a Leap Year.</h5>'; 
                    $counter++; 
                }else{   
                    echo '<h5>' . $startYear . '</h5>';
                }
            
                  $startYear++;
            }
            //Echo Total count of leap years
            echo '<h2 class="details">Total count of leap years from 1980 through 2018 equals' . $counter . '</h2>';
        ?>
    </main>        
</body>
</html>
